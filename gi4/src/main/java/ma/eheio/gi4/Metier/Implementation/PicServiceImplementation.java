package ma.eheio.gi4.Metier.Implementation;

import ma.eheio.gi4.Dao.Entity.Pic;
import ma.eheio.gi4.Dao.PicDao;
import ma.eheio.gi4.Metier.PicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PicServiceImplementation implements PicDao {

    @Autowired
    private PicService bs;

    @Override
    public List<Pic> findall() {
        return bs.findAll();
    }

    @Override
    public List<Pic> findByTheme(String mot) {
        return bs.findByTheme(mot);
    }

    @Override
    public String GenerateURL(String theme) {
        return null;
    }
}
