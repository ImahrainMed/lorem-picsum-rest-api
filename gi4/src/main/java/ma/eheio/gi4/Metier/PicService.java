package ma.eheio.gi4.Metier;

import ma.eheio.gi4.Dao.Entity.Pic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PicService extends JpaRepository<Pic, Long> {

    @Query("select p from Pic p where p.theme = :x")
    public List<Pic> findByTheme(@Param("x") String mot);

}
