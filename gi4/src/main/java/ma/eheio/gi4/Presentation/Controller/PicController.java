package ma.eheio.gi4.Presentation.Controller;

import ma.eheio.gi4.Dao.Entity.Pic;
import ma.eheio.gi4.Dao.PicDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("picsum")
@CrossOrigin(origins = "http://localhost:3000")
public class PicController {

    @Autowired
    private PicDao pd;

    @RequestMapping("/all")
    public ResponseEntity<List<Pic>> findAll(){
        return new ResponseEntity<>(pd.findall() , HttpStatus.OK);
    }

    @GetMapping( path = "/theme/{mot}")
    public ResponseEntity<List<Pic>> findByTheme(@PathVariable(value = "mot") String cle){
        return new ResponseEntity<>(pd.findByTheme(cle) , HttpStatus.OK);
    }


}
