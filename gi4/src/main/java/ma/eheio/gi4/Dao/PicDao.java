package ma.eheio.gi4.Dao;

import ma.eheio.gi4.Dao.Entity.Pic;

import java.util.List;

public interface PicDao {
    List<Pic> findall();
    List<Pic> findByTheme(String mot);
    String GenerateURL(String theme);
}
