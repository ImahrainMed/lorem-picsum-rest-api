package ma.eheio.gi4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gi4Application {

    public static void main(String[] args) {
        SpringApplication.run(Gi4Application.class, args);
    }

}
