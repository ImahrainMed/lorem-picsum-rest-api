import React, {Component} from 'react';
import {Jumbotron, Container , Navbar , Button } from "react-bootstrap";
import "./Header.css";

export default class Header extends Component {
    render() {

            return (
                <Navbar className="nvbr h-auto" bg="dark">
                    <Navbar.Brand href="/">
                        <img
                            src="/logo.svg"
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                            alt="RestFul Pics Api"
                        />
                        <h1>PicSum </h1>

                    </Navbar.Brand>
                    <Button style={{"text-align":"right"}} className="mll text-center" variant="success" ><a style={{"text-decoration":"none", "color":"white"}} href="/GenerateUrl">Get Started</a></Button>
                </Navbar>

            );
        }
    }


