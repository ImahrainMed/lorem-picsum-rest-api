import React, {Component} from 'react';
import {Container , Card ,Row , Col , Jumbotron , Button ,ButtonGroup} from 'react-bootstrap';
import axios from "axios";
import './WelcomePage.css'
import Header from "./Header";


export default class WelcomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            picture: []
        };
    }
    componentDidMount() {
        axios.get("http://localhost:8080/picsum/all")
            .then(response => response.data)
            .then((data)=>{
                this.setState({picture:data})
            })
    }

    render() {


        return (

          <div>

              <Jumbotron className="mt-5">
                  <h1> Lorem Picusm ,</h1>
                  <h3>
                     the Lorem Ipsum For Photos .

                  </h3>
                  <br/>
                  <p>
                      <ButtonGroup className="margin" style={{"margin-left":"30%"}}>
                          <Button   variant="info">3D</Button>
                          <Button  variant="info">Aero</Button>
                          <Button variant="info">Games</Button>
                          <Button  variant="info">Macro</Button>
                          <Button  variant="info">Pattern</Button>
                          <Button  variant="info">Sport</Button>
                          <Button  variant="info">Vector</Button>
                      </ButtonGroup>

                  </p>
              </Jumbotron>

              <Container  className="cont" >



                  <Row>
                      {

                          this.state.picture.map((p)=>(
                              <Col  >
                                  <Card key={p.id} style={{ width: '18rem' }}>
                                      <Card.Img variant="top" src={'/images/'+p.theme+'/'+p.srcname}   />

                                  </Card>



                              </Col>
                          ))




                      }
                  </Row>
              </Container>
          </div>

        );
    }
}

