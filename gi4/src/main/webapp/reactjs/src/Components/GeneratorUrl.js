import React, {Component} from 'react';
import {Jumbotron , Form , Button} from "react-bootstrap";
import axios from "axios";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCopy, faRecycle} from "@fortawesome/free-solid-svg-icons";

export default class GeneratorUrl extends Component {
    state= {
        value : ""
    };

  onChange = e =>{

      this.setState({value : e.target.value } );



  }


  onClick = e =>{
      /* Get the text field */
      var copyText = document.getElementById("myInput");

      /* Select the text field */
      copyText.select();
      copyText.setSelectionRange(0, 99999); /* For mobile devices */

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
  }


    render() {
      const {value} = this.state;
        const rand  = Math.floor(1+Math.random() * 11);


        return (
            <div>
                <Jumbotron className="mt-5 text-center">

                    <br/>
                    <select id="dropdown-basic-button" name="choix" className="custom-select" value={value} title="Choose Your Theme" onChange={this.onChange} >
                        <option value="3D">3D</option>
                        <option value="aero">Aero</option>
                        <option value="games">Games</option>
                        <option value="macro">Macro</option>
                        <option value="pattern">Pattern</option>
                        <option value="sport">Sport</option>
                        <option value="vector">Vector</option>

                    </select>
                    <br/>
                    <h3>
                        the Lorem Ipsum For Photos .

                    </h3>


                    <br/>
                    <input type="text" id="myInput" className="form-control text-center" onChange={this.onChange} value={"http://localhost:3000/images/"+this.state.value+"/"+rand+".jpg"}  />
                    <br/>

                    <Button variant="success" onClick={this.onClick}><FontAwesomeIcon icon={faCopy}/></Button>
                </Jumbotron>
            </div>
        );
    }
}
