import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router , Switch , Route} from 'react-router-dom';
import WelcomePage from "./Components/WelcomePage";
import {Container} from "react-bootstrap";
import Header from "./Components/Header";
import GeneratorUrl from "./Components/GeneratorUrl";
import React from "react";

function App() {
  return (
    <Router>
        <Header/>
      <Container>
        <Switch>
           <Route path="/" exact component={WelcomePage}/>
           <Route path="/GenerateUrl" exact component={GeneratorUrl}/>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
